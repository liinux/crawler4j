/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *     Author:eliteqing@foxmail.com
 * 
 */
package edu.uci.ics.crawler4j.tests.ziroom;

import java.util.List;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * @date 2016年8月20日 下午6:15:01
 * @version
 * @since JDK 1.8
 */
public class ZiroomController {

	public static void main(String[] args) {
		
		final String crawlStorageFolder = "data/crawl/root";
		final int numberOfCrawlers = 3;

		final CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(crawlStorageFolder);
		config.setPolitenessDelay(1000);
		config.setIncludeBinaryContentInCrawling(false);
		config.setMaxPagesToFetch(50);
		// config.setResumableCrawling(true);
		/*
		 * Instantiate the controller for this crawl.
		 */
		final PageFetcher pageFetcher = new PageFetcher(config);
		final RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		final RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
		CrawlController controller;
		try {
			controller = new CrawlController(config, pageFetcher, robotstxtServer);
			/*
			 * For each crawl, you need to add some seed urls. These are the
			 * first URLs that are fetched and then the crawler starts following
			 * links which are found in these pages
			 */
			controller.addSeed("http://sh.ziroom.com/z/nl/");
			// controller.addSeed("http://www.ziroom.com/z/nl/z3-u2.html/");
			// controller.addSeed("http://www.ics.uci.edu/~welling/");
			// controller.addSeed("http://www.ics.uci.edu/");

			/*
			 * Start the crawl. This is a blocking operation, meaning that your
			 * code will reach the line after this only when crawling is
			 * finished.
			 */
			controller.start(ZiroomCrawler.class, numberOfCrawlers);

			final List<Object> crawlersLocalData = controller.getCrawlersLocalData();
			long totalLinks = 0;
			long totalTextSize = 0;
			int totalProcessedPages = 0;
			for (final Object localData : crawlersLocalData) {
				final ZiroomCrawlStat stat = (ZiroomCrawlStat) localData;
				totalLinks += stat.getTotalLinks();
				totalTextSize += stat.getTotalTextSize();
				totalProcessedPages += stat.getTotalProcessedPages();
			}

			System.out.println("Aggregated Statistics:");
			System.out.println("\tProcessed Pages: {}" + totalProcessedPages);
			System.out.println("\tTotal Links found: {}" + totalLinks);
			System.out.println("\tTotal Text Size: {}" + totalTextSize);
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}